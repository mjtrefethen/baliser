package com.egoshard.baliser.core.repository;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.repository.jdbc.UserJdbcRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UUID;

/**
 *
 */
public class UnitTestUserJdbcRepositoruy {

    private final UserRepository repository = new UserJdbcRepository();
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testCreateNullParams() {
        thrown.expect(IllegalArgumentException.class);
        repository.create(null);
    }

    @Test
    public void testUpdateNullParam1() {
        thrown.expect(IllegalArgumentException.class);
        repository.update(null, new User());
    }

    @Test
    public void testUpdateNullParam2() {
        thrown.expect(IllegalArgumentException.class);
        repository.update(UUID.randomUUID(), null);
    }

    @Test
    public void testDeleteNullParam1() {
        thrown.expect(IllegalArgumentException.class);
        repository.delete(null, new User(UUID.randomUUID()));
    }

    @Test
    public void testDeleteNullParam2() {
        thrown.expect(IllegalArgumentException.class);
        repository.delete(UUID.randomUUID(), null);
    }

    @Test
    public void testFindNullParams() {
        thrown.expect(IllegalArgumentException.class);
        repository.findByKey(null);
    }

}
