package com.egoshard.baliser.core.repository.utils;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class UnitTestUuidUtils {

    private static UUID staticUuid;
    private static String staticUuidString = "4e08cf81-d452-4179-a9a2-06cd17ec7be5";

    @BeforeClass
    public static void setUpOnce() {
        staticUuid = UUID.randomUUID();
    }

    @Test
    public void testUuidValidity() {
        assertTrue(UuidUtils.isStringValidUuid(staticUuid.toString()));
    }

    @Test
    public void testByteUsage() {
        byte[] bytes = UuidUtils.toBytes(staticUuid);
        UUID localUuid = UuidUtils.fromBytes(bytes);
        assertEquals(staticUuid, localUuid);
    }

    @Test
    public void testNonDashString() {
        String uuidString = UuidUtils.toNonDashString(staticUuid);
        UUID localUuid = UuidUtils.fromNonDashString(uuidString);
        assertEquals(staticUuid, localUuid);
    }

    @Test
    public void testFullConversion() throws UnsupportedEncodingException {
        UUID existingUuid = UUID.fromString(staticUuidString);
        byte[] uuidBytes = UuidUtils.toBytes(existingUuid);
        UUID newUuid = UuidUtils.fromBytes(uuidBytes);
        assertEquals(existingUuid, newUuid);
        assertEquals(staticUuidString, newUuid.toString());
    }

}
