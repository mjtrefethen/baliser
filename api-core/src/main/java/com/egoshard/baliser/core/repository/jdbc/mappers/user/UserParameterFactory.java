package com.egoshard.baliser.core.repository.jdbc.mappers.user;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.repository.jdbc.mappers.ModelParameterFactory;
import com.egoshard.baliser.core.repository.jdbc.mappers.OperationType;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 */
public class UserParameterFactory extends ModelParameterFactory {

    /**
     * @param operation
     * @param callingId
     * @param user
     * @return
     */
    public static MapSqlParameterSource create(OperationType operation, long callingId, User user) {

        MapSqlParameterSource source;
        if (operation.equals(OperationType.CREATE)) {
            source = ModelParameterFactory.create(OperationType.CREATE, user);
            source.addValue("create_user", callingId);
        } else if (operation.equals(OperationType.UPDATE)) {
            source = ModelParameterFactory.create(OperationType.UPDATE, user);
            source.addValue("emailverified", user.isEmailVerified());
            source.addValue("active_flag", user.isActive());
        } else if (operation.equals(OperationType.DELETE)) {
            source = ModelParameterFactory.create(OperationType.UPDATE, user);
            source.addValue("modify_user", callingId);
            return source;
        } else {
            throw new IllegalArgumentException("An illegal operation type was passed to map parameters.");
        }
        // Common parameters
        source.addValue("givenname", user.getGivenName());
        source.addValue("surname", user.getSurname());
        source.addValue("email", user.getEmail());
        source.addValue("modify_user", callingId);
        return source;

    }

}
