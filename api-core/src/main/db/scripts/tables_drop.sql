/*

  */

DROP TABLE IF EXISTS `user_fence`;
DROP TABLE IF EXISTS `user_group`;
DROP TABLE IF EXISTS `device_group`;
DROP TABLE IF EXISTS `group_fence`;
DROP TABLE IF EXISTS `device`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `user_type`;
DROP TABLE IF EXISTS `fence`;
DROP TABLE IF EXISTS `fence_type`;
DROP TABLE IF EXISTS `group`;