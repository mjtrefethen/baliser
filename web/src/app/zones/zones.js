angular
    .module('baliserApp.zones', [
        'ui.router'
    ])

    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('zones', {
            url: '/zones',
            views: {
                "main": {
                    controller: 'ZonesCtrl',
                    templateUrl: 'zones/zones.tpl.html'
                }
            },
            data: { pageTitle: "Zones" }
        });
    }])

    .controller('ZonesCtrl', ['$scope', function GroupsCtrl($scope) {

    }]);