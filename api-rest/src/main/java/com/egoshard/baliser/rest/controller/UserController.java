package com.egoshard.baliser.rest.controller;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.events.ModelReadEvent;
import com.egoshard.baliser.core.events.ModelWrittenEvent;
import com.egoshard.baliser.core.events.ReadModelEvent;
import com.egoshard.baliser.core.events.WriteModelEvent;
import com.egoshard.baliser.core.events.user.CreateUserEvent;
import com.egoshard.baliser.core.repository.utils.UuidUtils;
import com.egoshard.baliser.core.service.UserService;
import com.egoshard.baliser.rest.domain.assembler.UserAssembler;
import com.egoshard.baliser.rest.domain.user.UserResource;
import com.wordnik.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

/**
 *
 */
@Api(value = "users", description = "")
@Produces(value = MediaType.APPLICATION_JSON)
@RestController
@RequestMapping("/users")
public class UserController {

    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private UserService userService;

    /**
     * @param userService
     */
    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param key
     * @return
     */
    @ApiOperation(
            value = "Find User by unique identifier",
            notes = "Retrieves a specific User record for a unique record identifier.",
            response = UserResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "The unique key for the specified resource is invalid."),
            @ApiResponse(code = 401, message = "The current authenticated user does not permissions to interact with the specified resource."),
            @ApiResponse(code = 404, message = "No resource was found for the unique key specified."),
            @ApiResponse(code = 200, message = "Success")})
    @RequestMapping(value = "/{key}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<UserResource> getForKey(
            @ApiParam(value = "Unique identifier of the User to fetch", required = true) @PathVariable String key) {

        if (key == null || UuidUtils.isStringValidUuid(key)) {
            throw new IllegalArgumentException("The identifier specified appears to be invalid, please verify and resubmit the request.");
        }

        ModelReadEvent<User> event = userService.requestUser(new ReadModelEvent(/* TODO authentication */UUID.fromString("4e08cf81-d452-4179-a9a2-06cd17ec7be5"), UuidUtils.fromNonDashString(key)));
        return new ResponseEntity<>(UserAssembler.toResource(event.getModel()), HttpStatus.OK);

    }

    /**
     * @param userResource
     * @param password
     * @return
     */
    @ApiOperation(
            value = "Add User entity",
            notes = "Creates a new User record.",
            response = UserResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "The user data specified in the request invalid."),
            @ApiResponse(code = 401, message = "The current authenticated user does not permissions to interact with the specified resource."),
            @ApiResponse(code = 409, message = "A user with the specified email address already exists."),
            @ApiResponse(code = 201, message = "A new user was created.")})
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<UserResource> createUser(
            @ApiParam(value = "User entity to create", required = true) @RequestBody UserResource userResource,
            @ApiParam(value = "Password to associate with the newly created user", required = true) @RequestBody String password) {

        if (userResource == null || password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Required user data appears to be missing from the request, please verify and resubmit the request.");
        }

        ModelWrittenEvent<User> event = userService.createUser(new CreateUserEvent(/* TODO authentication */UUID.fromString("4e08cf81-d452-4179-a9a2-06cd17ec7be5"), UserAssembler.toModel(userResource), password));
        return new ResponseEntity<>(UserAssembler.toResource(event.getModel()), HttpStatus.CREATED);

    }

    /**
     * @return
     */
    @ApiOperation(
            value = "Update User entity",
            notes = "Updates an existing User record.",
            response = UserResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "The unique key for the specified resource is invalid."),
            @ApiResponse(code = 400, message = "The user data specified in the request invalid."),
            @ApiResponse(code = 401, message = "The current authenticated user does not permissions to interact with the specified resource."),
            @ApiResponse(code = 404, message = "No resource was found for the unique key specified."),
            @ApiResponse(code = 200, message = "Success")})
    @RequestMapping(value = "/{key}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<UserResource> updateUser(
            @ApiParam(value = "Unique identifier of the User to fetch", required = true) @PathVariable String key,
            @ApiParam(value = "User entity to create", required = true) @RequestBody UserResource userResource) {

        if (key == null || UuidUtils.isStringValidUuid(key)) {
            throw new IllegalArgumentException("The identifier specified appears to be invalid, please verify and resubmit the request.");
        }
        if (userResource == null) {
            throw new IllegalArgumentException("Required user data appears to be missing from the request, please verify and resubmit the request.");
        }

        ModelWrittenEvent<User> event = userService.updateUser(new WriteModelEvent<>(/* TODO authentication */UUID.fromString("4e08cf81-d452-4179-a9a2-06cd17ec7be5"), UserAssembler.toModel(userResource)));
        return new ResponseEntity<>(UserAssembler.toResource(event.getModel()), HttpStatus.OK);

    }

    /**
     * @return
     */
    @ApiOperation(
            value = "Delete User entity",
            notes = "Deletes an existing User record.",
            response = UserResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "The unique key for the specified resource is invalid."),
            @ApiResponse(code = 400, message = "The user data specified in the request invalid."),
            @ApiResponse(code = 401, message = "The current authenticated user does not permissions to interact with the specified resource."),
            @ApiResponse(code = 404, message = "No resource was found for the unique key specified."),
            @ApiResponse(code = 200, message = "Success")})
    @RequestMapping(value = "/{key}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<UserResource> deleteUser(
            @ApiParam(value = "Unique identifier of the User to fetch", required = true) @PathVariable String key,
            @ApiParam(value = "User entity to create", required = true) @RequestBody UserResource userResource) {

        if (key == null || UuidUtils.isStringValidUuid(key)) {
            throw new IllegalArgumentException("The identifier specified appears to be invalid, please verify and resubmit the request.");
        }
        if (userResource == null) {
            throw new IllegalArgumentException("Required user data appears to be missing from the request, please verify and resubmit the request.");
        }

        ModelWrittenEvent<User> event = userService.deleteUser(new WriteModelEvent<>(/* TODO authentication */UUID.fromString("4e08cf81-d452-4179-a9a2-06cd17ec7be5"), UserAssembler.toModel(userResource)));
        return new ResponseEntity<>(UserAssembler.toResource(event.getModel()), HttpStatus.OK);

    }

}
