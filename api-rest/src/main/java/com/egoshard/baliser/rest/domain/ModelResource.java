package com.egoshard.baliser.rest.domain;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 */
@ApiModel("Default Model resource object.")
public class ModelResource extends ResourceSupport implements Serializable {

    @ApiModelProperty(value = "")
    private UUID key;
    private String name;

    /**
     *
     */
    public ModelResource() {
    }

    /**
     * @param key
     * @param name
     */
    public ModelResource(UUID key, String name) {
        this.key = key;
        this.name = name;
    }

    /**
     * @return
     */
    public UUID getKey() {
        return key;
    }

    /**
     * @param key
     */
    public void setKey(UUID key) {
        this.key = key;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

}
