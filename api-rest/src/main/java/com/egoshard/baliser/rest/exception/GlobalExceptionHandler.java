package com.egoshard.baliser.rest.exception;

import com.egoshard.baliser.core.repository.exception.ModelNotFoundException;
import com.egoshard.baliser.core.repository.exception.ModelNotUniqueException;
import com.egoshard.baliser.core.repository.exception.UnauthorizedException;
import com.egoshard.baliser.rest.JsonErrorInfo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * @param request
     * @param ex
     * @return
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ModelAndView defaultHandler(HttpServletRequest request, Exception ex) {
        return assembleView(request, ex);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public ModelAndView illegalArgument(HttpServletRequest request, Exception ex) {
        return assembleView(request, ex.toString());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NumberFormatException.class)
    public ModelAndView illegalArgumentCast(HttpServletRequest request, Exception ex) {
        return assembleView(request, "A request parameter contains data of the wrong datatype. Please verify parameters and reattempt.");
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ModelNotFoundException.class)
    public ModelAndView entityMissing(HttpServletRequest request, Exception ex) {
        return assembleView(request, ex.toString());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ModelNotUniqueException.class)
    public ModelAndView entityNotUnique(HttpServletRequest request, Exception ex) {
        return assembleView(request, ex.toString());
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UnauthorizedException.class)
    public ModelAndView unauthorizedAccess(HttpServletRequest request, Exception ex) {
        return assembleView(request, ex.toString());
    }


    /**
     * @param request
     * @param ex
     * @return
     */
    private ModelAndView assembleView(HttpServletRequest request, Exception ex) {
        return assembleView(request, ex.getMessage());
    }

    /**
     * @param request
     * @param message
     * @return
     */
    private ModelAndView assembleView(HttpServletRequest request, String message) {
        JsonErrorInfo info = new JsonErrorInfo();
        info.getValues().put("url", request.getRequestURL().toString());
        info.getValues().put("error", message);
        return info.asModelAndView();
    }

}