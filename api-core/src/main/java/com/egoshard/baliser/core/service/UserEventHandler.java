package com.egoshard.baliser.core.service;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.events.ModelReadEvent;
import com.egoshard.baliser.core.events.ModelWrittenEvent;
import com.egoshard.baliser.core.events.ReadModelEvent;
import com.egoshard.baliser.core.events.WriteModelEvent;
import com.egoshard.baliser.core.events.user.CreateUserEvent;
import com.egoshard.baliser.core.repository.UserRepository;
import com.egoshard.baliser.core.repository.exception.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class UserEventHandler implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserEventHandler.class);
    private final UserRepository userRepository;

    @Autowired
    public UserEventHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Mechanism to create a new user.
     *
     * @param event populated create event.
     * @return newly created Model object and associated key.
     */
    @Override
    public ModelWrittenEvent<User> createUser(final CreateUserEvent event) {

        // TODO validate object
        return new ModelWrittenEvent<>(userRepository.create(event.getModel()));
    }

    /**
     * Request a user Model object by its unique identifier.
     *
     * @param event populated request event.
     * @return populated return event.
     */
    @Override
    public ModelReadEvent<User> requestUser(final ReadModelEvent event) {

        // TODO validate object
        if (!event.getExecutorKey().equals(event.getKey())) {
            // Currently read requests may only be performed by users against their own User entity.
            throw new UnauthorizedException("The authenticated user does not have permission to access the requested entity.");
        }
        return new ModelReadEvent<>(userRepository.findByKey(event.getKey()));
    }

    /**
     * Mechanism to update an existing user.
     *
     * @param event populated update event.
     * @return modified Model object and associated key.
     */
    @Override
    public ModelWrittenEvent<User> updateUser(final WriteModelEvent<User> event) {
        // TODO valdate object
        if (!event.getExecutorKey().equals(event.getModel().getKey())) {
            // Currently write requests may only be performed by users against their own User entity.
            throw new UnauthorizedException("The authenticated user does not have permission to access the requested entity.");
        }
        return new ModelWrittenEvent<>(userRepository.update(event.getExecutorKey(), event.getModel()));
    }

    /**
     * Mechanism to delete an existing user.
     *
     * @param event populated delete event.
     * @return event indicating successful deletion..
     */
    @Override
    public ModelWrittenEvent<User> deleteUser(final WriteModelEvent<User> event) {
        // TODO valdate object
        if (!event.getExecutorKey().equals(event.getModel().getKey())) {
            // Currently write requests may only be performed by users against their own User entity.
            throw new UnauthorizedException("The authenticated user does not have permission to access the requested entity.");
        }
        return new ModelWrittenEvent<>(userRepository.delete(event.getExecutorKey(), event.getModel()));
    }

}
