package com.egoshard.baliser.core.service;

import com.egoshard.baliser.core.domain.user.User;

/**
 * Service interface
 */
public interface AuthenticationService {

    public User authenticate();

}
