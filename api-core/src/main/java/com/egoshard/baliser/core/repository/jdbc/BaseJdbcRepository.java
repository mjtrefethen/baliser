package com.egoshard.baliser.core.repository.jdbc;

import com.egoshard.baliser.core.repository.exception.ModelNotFoundException;
import com.egoshard.baliser.core.repository.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.UUID;

/**
 *
 */
class BaseJdbcRepository {

    protected static final UUID SYSTEM_KEY = UUID.fromString("4e08cf81-d452-4179-a9a2-06cd17ec7be5");
    private static final String GET_RECORD_ID_SQL = "SELECT record_id FROM user WHERE record_key = :record_key";
    protected NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * @param key
     * @return
     */
    @Cacheable("userLookupCache")
    protected Long getRecordId(UUID key) {
        Long id;
        try {
            id = this.jdbcTemplate.queryForObject(GET_RECORD_ID_SQL, new MapSqlParameterSource("record_key", UuidUtils.toBytes(key)), Long.class);
        } catch (IncorrectResultSizeDataAccessException ex) {
            throw new ModelNotFoundException("A User model cannot be found for the specified key, " + key.toString());
        }
        return id;
    }

}
