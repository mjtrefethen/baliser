package com.egoshard.baliser.core.events.user;

import com.egoshard.baliser.core.events.BaseEvent;

/**
 * Request to update a specified user password hash value.
 */
public class UpdatePasswordEvent extends BaseEvent {

    private final String key;
    private final String passwordHash;

    /**
     * Constructor to build a password hash change event.
     *
     * @param key          unique identifier for the user.
     * @param passwordHash new password hash.
     */
    public UpdatePasswordEvent(final String key, final String passwordHash) {
        this.key = key;
        this.passwordHash = passwordHash;
    }

    /**
     * Retrieves the unique identifier of the user.
     *
     * @return unique identifier.
     */
    public String getKey() {
        return key;
    }

    /**
     * Retrieves the new password hash
     *
     * @return String value of the new password hash.
     */
    public String getPasswordHash() {
        return passwordHash;
    }

}
