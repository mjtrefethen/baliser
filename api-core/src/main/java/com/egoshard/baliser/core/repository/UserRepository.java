package com.egoshard.baliser.core.repository;

import com.egoshard.baliser.core.domain.user.User;

import java.util.UUID;

/**
 * Describes the functionality for persisting User Model objects.
 */
public interface UserRepository {

    /**
     * @param user
     * @return
     */
    public User create(final User user);

    /**
     * Retrieves a specified User Model object from persistent storage.
     *
     * @param key unique identifier of the object to retrieve.
     * @return populated User object.
     */
    public User findByKey(UUID key);

    /**
     * Updates a User object in persistent storage.
     *
     * @param callingKey unique identifier of the calling user.
     * @param user       Model object to be saved.
     * @return User object populated with generated identifiers.
     */
    public User update(final UUID callingKey, final User user);

    /**
     * Deletes a User Model object.
     *
     * @param callingKey unique identifier of the calling user.
     * @param user       Model object to be deleted..
     */
    public User delete(final UUID callingKey, final User user);

}