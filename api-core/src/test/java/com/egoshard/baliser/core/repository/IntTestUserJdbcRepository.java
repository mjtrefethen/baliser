package com.egoshard.baliser.core.repository;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.repository.exception.ModelNotFoundException;
import com.egoshard.baliser.core.repository.exception.ModelNotUniqueException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;


/**
 *
 */
@ContextConfiguration("classpath:test-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class IntTestUserJdbcRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(IntTestUserJdbcRepository.class);
    private static final User staticUser = new User();
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private UserRepository userRepository;
    private TestBaseRepository testRepository;

    public IntTestUserJdbcRepository() {
    }

    @Before
    public void setUp() {
        staticUser.setEmail("test@test.com");
        staticUser.setGivenName("FirstName");
        staticUser.setSurname("LastName");
        staticUser.setEmailVerified(false);
        staticUser.setActive(true);
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setTestRepository(TestBaseRepository testRepository) {
        this.testRepository = testRepository;
    }

    @Test
    public void testCreateUser() {
        User newUser = userRepository.create(staticUser);
        assertEquals(staticUser, newUser);
    }

    @Test
    public void testCreateUserDuplicate() {
        userRepository.create(staticUser);
        thrown.expect(ModelNotUniqueException.class);
        userRepository.create(staticUser);
    }

    @Test
    public void testUpdateUser() {
        userRepository.create(staticUser);

        User updated;
        staticUser.setGivenName("NewGivenName");
        updated = userRepository.update(staticUser.getKey(), staticUser);
        assertEquals(staticUser, updated);

        staticUser.setSurname("NewSurname");
        updated = userRepository.update(staticUser.getKey(), staticUser);
        assertEquals(staticUser, updated);

        staticUser.setEmail("NewEmail");
        updated = userRepository.update(staticUser.getKey(), staticUser);
        assertEquals(staticUser, updated);

        staticUser.setEmailVerified(true);
        updated = userRepository.update(staticUser.getKey(), staticUser);
        assertEquals(staticUser, updated);

        staticUser.setActive(false);
        updated = userRepository.update(staticUser.getKey(), staticUser);
        assertEquals(staticUser, updated);
    }

    @Test
    public void testUpdateMissingUser() {
        userRepository.create(staticUser);
        User missingUser = new User();
        missingUser.setGivenName("Test");
        missingUser.setSurname("Test");
        missingUser.setEmail("Test");
        thrown.expect(ModelNotFoundException.class);
        userRepository.update(staticUser.getKey(), missingUser);
    }

    @Test
    public void testDeleteUser() {
        userRepository.create(staticUser);
        userRepository.delete(staticUser.getKey(), staticUser);
        User inactiveUser = userRepository.findByKey(staticUser.getKey());
        assertFalse(inactiveUser.isActive());
    }

    @Test
    public void testDeleteMissingUser() {
        thrown.expect(ModelNotFoundException.class);
        userRepository.delete(staticUser.getKey(), new User(UUID.randomUUID()));
    }

    @Test
    public void testFind() {
        userRepository.create(staticUser);
        User newUser = userRepository.findByKey(staticUser.getKey());
        assertEquals(staticUser, newUser);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testFindMissing() {
        User newUser = userRepository.findByKey(staticUser.getKey());
    }

    /**
     * Empty all test users after each test.
     */
    @After
    public void afterTest() {
        this.testRepository.getJdbcTemplate().update("DELETE FROM user WHERE record_id > :minRecord", new MapSqlParameterSource("minRecord", 1));
    }

}
