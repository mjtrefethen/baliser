package com.egoshard.baliser.core.domain;

import java.util.UUID;

/**
 * Base abstract representation of a database record.
 * All extensions of Model will have a UUID identifier and a String
 * representation of a human readable name. Extending classes must implement
 * getters and setters for a Name property.
 */
public abstract class Model {

    private UUID key;
    private boolean active;

    /**
     * Default constructor for all Model objects.
     * <p/>
     * <p>
     * This is used primarily to create new record objects that are subsequently saved in the database.
     * </p>
     */
    public Model() {
        this.key = UUID.randomUUID();
    }

    /**
     * Constructor used to populate record retrieved from storage.
     *
     * @param key unique public identifier.
     */
    public Model(final UUID key) {
        this.key = key;
    }

    /**
     * Gets the public UUID for this record.
     *
     * @return unique identifier for this record object.
     */
    public UUID getKey() {
        return key;
    }

    /**
     * Gets the string representation of this record.
     *
     * @return String representing the primary display value of this record.
     */
    public abstract String getName();

    /**
     * @return
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Model model = (Model) o;

        if (active != model.active) return false;
        if (key != null ? !key.equals(model.key) : model.key != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Model{" +
                "key=" + key +
                ", active=" + active +
                '}';
    }

}