package com.egoshard.baliser.core.events.user;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.events.WriteModelEvent;

import java.util.UUID;

/**
 * Event to create a new user.
 */
public class CreateUserEvent extends WriteModelEvent<User> {

    private final String password;

    /**
     * Constructor to create a new user event.
     *
     * @param executorKey unique identifier for the user requesting access.
     * @param user        User to create.
     * @param password    Password hash value to associate to the new user.
     */
    public CreateUserEvent(final UUID executorKey, final User user, final String password) {
        super(executorKey, user);
        this.password = password;
    }

    /**
     * Retrieves the password hash for the new user.
     *
     * @return hashed string value of the users password.
     */
    public String getPassword() {
        return password;
    }

}
