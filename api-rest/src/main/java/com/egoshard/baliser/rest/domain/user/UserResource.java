package com.egoshard.baliser.rest.domain.user;

import com.egoshard.baliser.rest.domain.ModelResource;
import com.wordnik.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 */
@ApiModel("User")
public class UserResource extends ModelResource implements Serializable {

    private String givenName;
    private String surname;
    private String email;
    private boolean emailVerified;

    /**
     *
     */
    public UserResource() {
    }

    /**
     * @param key
     * @param name
     */
    public UserResource(UUID key, String name) {
        super(key, name);
    }

    /**
     * @return
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * @param givenName
     */
    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    /**
     * @return
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return
     */
    public boolean isEmailVerified() {
        return emailVerified;
    }

    /**
     * @param emailVerified
     */
    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

}
