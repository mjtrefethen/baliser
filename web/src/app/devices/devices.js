angular
    .module('baliserApp.devices', [
        'ui.router'
    ])

    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('devices', {
            url: '/devices',
            views: {
                "main": {
                    controller: 'DevicesCtrl',
                    templateUrl: 'devices/devices.tpl.html'
                }
            },
            data: { pageTitle: "Devices" }
        });
    }])

    .controller('DevicesCtrl', ['$scope', function DevicesCtrl($scope) {

    }]);