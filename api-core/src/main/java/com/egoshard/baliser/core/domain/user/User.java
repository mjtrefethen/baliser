package com.egoshard.baliser.core.domain.user;

import com.egoshard.baliser.core.domain.Model;

import java.util.UUID;

/**
 *
 */
public class User extends Model {

    private String givenName;
    private String surname;
    private String email;
    private boolean emailVerified;

    /**
     * Default constructor for all Model objects.
     * <p/>
     * <p>
     * This is used primarily to create new record objects that are subsequently saved in the database.
     * </p>
     */
    public User() {
        super();
    }

    /**
     * Constructor used to populate record retrieved from storage.
     *
     * @param uuid unique public identifier.
     */
    public User(final UUID uuid) {
        super(uuid);
    }

    /**
     * @return
     */
    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    /**
     * Gets the string representation of this record.
     *
     * @return String concatenation of the given and sur names of the user.
     */
    @Override
    public String getName() {
        // TODO This should allow for differing formats based on locale.
        return this.getGivenName() + " " + this.getSurname();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        if (emailVerified != user.emailVerified) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (givenName != null ? !givenName.equals(user.givenName) : user.givenName != null) return false;
        if (surname != null ? !surname.equals(user.surname) : user.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (givenName != null ? givenName.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (emailVerified ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "givenName='" + givenName + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", emailVerified=" + emailVerified +
                "} " + super.toString();
    }

    /**
     *
     */
    public enum UserType {

        System(1, "System"),
        Administrator(2, "Administrator"),
        User(3, "User");
        private final long id;
        private final String name;

        /**
         * @param id
         * @param name
         */
        private UserType(long id, String name) {
            this.id = id;
            this.name = name;
        }

        /**
         * @return
         */
        public long getId() {
            return id;
        }

        /**
         * @return
         */
        public String getName() {
            return name;
        }

    }

}
