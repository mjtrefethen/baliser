angular
    .module('baliserApp.groups', [
        'ui.router'
    ])

    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('groups', {
            url: '/groups',
            views: {
                "main": {
                    controller: 'GroupsCtrl',
                    templateUrl: 'groups/groups.tpl.html'
                }
            },
            data: { pageTitle: "Groups" }
        });
    }])

    .controller('GroupsCtrl', ['$scope', function GroupsCtrl($scope) {

    }]);