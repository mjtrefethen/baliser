/*

  */

-- -----------------------------------------------------
-- Table `fence_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fence_type` (
  `record_id` BIGINT       NOT NULL,
  `name`      VARCHAR(255) NOT NULL,
  PRIMARY KEY (`record_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fence` (
  `record_id`      BIGINT       NOT NULL AUTO_INCREMENT,
  `record_key`     BINARY(16)   NOT NULL,
  `fence_type_rid` BIGINT       NOT NULL,
  `name`           VARCHAR(255) NOT NULL,
  `geoJson`        TEXT         NULL,
  PRIMARY KEY (`record_id`),
  CONSTRAINT `fk_fence_fence_type`
  FOREIGN KEY (`fence_type_rid`)
  REFERENCES `fence_type` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `ix_fence_key` ON `fence` (`record_key` ASC);

CREATE INDEX `fk_fence_fence_type` ON `fence` (`fence_type_rid` ASC);


-- -----------------------------------------------------
-- Table `user_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_type` (
  `record_id` BIGINT       NOT NULL,
  `name`      VARCHAR(255) NOT NULL,
  PRIMARY KEY (`record_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `record_id`     BIGINT       NOT NULL AUTO_INCREMENT,
  `record_key`    BINARY(16)   NOT NULL,
  `user_type_rid` BIGINT       NOT NULL DEFAULT 3,
  `givenname`     VARCHAR(255) NOT NULL,
  `surname`       VARCHAR(255) NOT NULL,
  `email`         VARCHAR(255) NOT NULL,
  `emailverified` TINYINT(1)   NOT NULL DEFAULT 0,
  `password_hash` BINARY(60)   NULL,
  `create_user`   BIGINT       NOT NULL,
  `create_dt`     TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user`   BIGINT       NOT NULL,
  `modify_dt`     TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_flag`   TINYINT(1)   NOT NULL DEFAULT 1,
  PRIMARY KEY (`record_id`),
  CONSTRAINT `fk_user_user_type`
  FOREIGN KEY (`user_type_rid`)
  REFERENCES `user_type` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2;

CREATE INDEX `ix_user_key` ON `user` (`record_key` ASC);

CREATE INDEX `ix_user_email` ON `user` (`email` ASC);

CREATE UNIQUE INDEX `ux_email` ON `user` (`email` ASC);

CREATE INDEX `fk_user_user_type` ON `user` (`user_type_rid` ASC);


-- -----------------------------------------------------
-- Table `device`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `device` (
  `record_id`  BIGINT     NOT NULL AUTO_INCREMENT,
  `record_key` BINARY(16) NULL,
  `user_rid`   BIGINT     NOT NULL,
  PRIMARY KEY (`record_id`),
  CONSTRAINT `fk_device_user`
  FOREIGN KEY (`user_rid`)
  REFERENCES `user` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `ix_device_key` ON `device` (`record_key` ASC);

CREATE INDEX `fk_device_user` ON `device` (`user_rid` ASC);


-- -----------------------------------------------------
-- Table `group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `group` (
  `record_id`  BIGINT     NOT NULL AUTO_INCREMENT,
  `record_key` BINARY(16) NOT NULL,
  PRIMARY KEY (`record_id`))
  ENGINE = InnoDB;

CREATE INDEX `ix_group_key` ON `group` (`record_key` ASC);


-- -----------------------------------------------------
-- Table `user_fence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_fence` (
  `user_record_id`  BIGINT NOT NULL,
  `fence_record_id` BIGINT NOT NULL,
  CONSTRAINT `fk_user_fence_user`
  FOREIGN KEY (`user_record_id`)
  REFERENCES `user` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_fence_fence`
  FOREIGN KEY (`fence_record_id`)
  REFERENCES `fence` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `fk_user_fence_user` ON `user_fence` (`user_record_id` ASC);

CREATE INDEX `fk_user_fence_fence` ON `user_fence` (`fence_record_id` ASC);


-- -----------------------------------------------------
-- Table `user_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_group` (
  `user_rid`  BIGINT NOT NULL,
  `group_rid` BIGINT NOT NULL,
  CONSTRAINT `fk_user_group_user`
  FOREIGN KEY (`user_rid`)
  REFERENCES `user` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_group_group`
  FOREIGN KEY (`group_rid`)
  REFERENCES `group` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `fk_user_group_user` ON `user_group` (`user_rid` ASC);

CREATE INDEX `fk_user_group_group` ON `user_group` (`group_rid` ASC);


-- -----------------------------------------------------
-- Table `group_fence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `group_fence` (
  `group_rid` BIGINT NOT NULL,
  `fence_rid` BIGINT NOT NULL,
  CONSTRAINT `fk_group_fence_group`
  FOREIGN KEY (`group_rid`)
  REFERENCES `group` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_fence_fence`
  FOREIGN KEY (`fence_rid`)
  REFERENCES `fence` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `fk_group_fence_group` ON `group_fence` (`group_rid` ASC);

CREATE INDEX `fk_group_fence_fence` ON `group_fence` (`fence_rid` ASC);


-- -----------------------------------------------------
-- Table `device_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `device_group` (
  `device_rid` BIGINT NOT NULL,
  `group_rid`  BIGINT NOT NULL,
  CONSTRAINT `fk_device_group_device`
  FOREIGN KEY (`device_rid`)
  REFERENCES `device` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_device_group_group`
  FOREIGN KEY (`group_rid`)
  REFERENCES `group` (`record_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `fk_device_group_device` ON `device_group` (`device_rid` ASC);

CREATE INDEX `fk_device_group_group` ON `device_group` (`group_rid` ASC);