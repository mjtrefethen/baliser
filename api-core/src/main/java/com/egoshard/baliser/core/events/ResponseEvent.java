package com.egoshard.baliser.core.events;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ResponseEvent extends BaseEvent {

    private final List<Exception> exceptions = new ArrayList<Exception>();

    /**
     * @return
     */
    public List<Exception> getExceptions() {
        return exceptions;
    }

    /**
     * @return
     */
    public boolean isSuccess() {
        return exceptions.size() == 0;
    }

}
