-- -----------------------------------------------------
-- Data for table `fence_type`
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO `fence_type` (`record_id`, `name`) VALUES (1, 'Should Not Leave');
INSERT INTO `fence_type` (`record_id`, `name`) VALUES (2, 'Should Not Enter');

COMMIT;

-- -----------------------------------------------------
-- Data for table `user_type`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `user_type` (`record_id`, `name`) VALUES (1, 'System');
INSERT INTO `user_type` (`record_id`, `name`) VALUES (2, 'Administrator');
INSERT INTO `user_type` (`record_id`, `name`) VALUES (3, 'User');

COMMIT;

-- -----------------------------------------------------
-- Data for table `user`
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO `user` (`record_id`, `record_key`, `user_type_rid`, `givenname`, `surname`, `email`, `emailverified`, `create_user`, `modify_user`) VALUES (1, UNHEX(
    '4E08CF81D4524179A9A206CD17EC7BE5'), 1, 'System', 'User', 'system@egoshard.com', 1, 1, 1);

COMMIT;
