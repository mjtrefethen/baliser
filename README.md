# Baliser #
Verb, *To mark something with beacons or signposts.*
### Description ###

REST service implementation of a geofencing application for reporting the activity of mobile devices. The web application and underlying service allow for the creation of fences and the application of a rules engine for notification when registered mobile devices transition into and out of fenced areas.