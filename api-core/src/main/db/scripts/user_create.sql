/*

 */
CREATE USER 'baliser_user'
  IDENTIFIED BY 'b@li53r_u53r';

GRANT SELECT ON TABLE 'baliser'.'fence' TO baliser_user;
GRANT UPDATE ON TABLE 'baliser'.'fence' TO baliser_user;
GRANT DELETE ON TABLE 'baliser'.'fence' TO baliser_user;
GRANT INSERT ON TABLE 'baliser'.'fence' TO baliser_user;
GRANT DELETE ON TABLE 'baliser'.'user' TO baliser_user;
GRANT INSERT ON TABLE 'baliser'.'user' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'user' TO baliser_user;
GRANT UPDATE ON TABLE 'baliser'.'user' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'fence_type' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'device' TO baliser_user;
GRANT UPDATE ON TABLE 'baliser'.'device' TO baliser_user;
GRANT DELETE ON TABLE 'baliser'.'device' TO baliser_user;
GRANT INSERT ON TABLE 'baliser'.'device' TO baliser_user;
GRANT DELETE ON TABLE 'baliser'.'device_group' TO baliser_user;
GRANT INSERT ON TABLE 'baliser'.'device_group' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'device_group' TO baliser_user;
GRANT UPDATE ON TABLE 'baliser'.'device_group' TO baliser_user;
GRANT DELETE ON TABLE 'baliser'.'group' TO baliser_user;
GRANT INSERT ON TABLE 'baliser'.'group' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'group' TO baliser_user;
GRANT UPDATE ON TABLE 'baliser'.'group' TO baliser_user;
GRANT DELETE ON TABLE 'baliser'.'user_group' TO baliser_user;
GRANT INSERT ON TABLE 'baliser'.'user_group' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'user_group' TO baliser_user;
GRANT UPDATE ON TABLE 'baliser'.'user_group' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'user_fence' TO baliser_user;
GRANT UPDATE ON TABLE 'baliser'.'user_fence' TO baliser_user;
GRANT INSERT ON TABLE 'baliser'.'user_fence' TO baliser_user;
GRANT DELETE ON TABLE 'baliser'.'user_fence' TO baliser_user;
GRANT DELETE ON TABLE 'baliser'.'group_fence' TO baliser_user;
GRANT INSERT ON TABLE 'baliser'.'group_fence' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'group_fence' TO baliser_user;
GRANT UPDATE ON TABLE 'baliser'.'group_fence' TO baliser_user;
GRANT SELECT ON TABLE 'baliser'.'user_type' TO baliser_user;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;