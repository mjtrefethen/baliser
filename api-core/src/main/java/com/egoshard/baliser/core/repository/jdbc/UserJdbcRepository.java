package com.egoshard.baliser.core.repository.jdbc;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.repository.UserRepository;
import com.egoshard.baliser.core.repository.exception.ModelNotFoundException;
import com.egoshard.baliser.core.repository.exception.ModelNotUniqueException;
import com.egoshard.baliser.core.repository.jdbc.mappers.ModelParameterFactory;
import com.egoshard.baliser.core.repository.jdbc.mappers.OperationType;
import com.egoshard.baliser.core.repository.jdbc.mappers.user.UserParameterFactory;
import com.egoshard.baliser.core.repository.jdbc.mappers.user.UserRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 *
 */
@Repository
public class UserJdbcRepository extends BaseJdbcRepository implements UserRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserJdbcRepository.class);

    /**
     * Saves a User object to persistent storage.
     *
     * @param user Model object to be saved.
     * @return User object populated with generated identifiers.
     */
    @Override
    public User create(final User user) {

        if (user == null) {
            throw new IllegalArgumentException("A User model is required.");
        }
        if (userExists(user.getEmail())) {
            throw new ModelNotUniqueException("A user already exists for the specified email address.");
        }

        // Calling user is System since registrations are public operations.
        Long callingId = this.getRecordId(SYSTEM_KEY);
        jdbcTemplate.update(
                "INSERT INTO user (record_key, givenname, surname, email, create_user, modify_user) VALUES (:record_key, :givenname, :surname, :email, :create_user, :modify_user)",
                UserParameterFactory.create(OperationType.CREATE, callingId, user)
        );
        return findByKey(user.getKey());
    }

    /**
     * Updates a User object in persistent storage.
     *
     * @param callingKey unique identifier of the calling user.
     * @param user       Model object to be saved.
     * @return User object populated with generated identifiers.
     */
    @Override
    public User update(final UUID callingKey, final User user) {
        if (callingKey == null) {
            throw new IllegalArgumentException("CallingKey is a required argument.");
        }
        if (user == null) {
            throw new IllegalArgumentException("User is a required argument.");
        }
        if (!userExists(user.getKey())) {
            throw new ModelNotFoundException("A User model cannot be found for the specified key, " + user.getKey().toString());
        }

        Long callingId = this.getRecordId(callingKey);
        jdbcTemplate.update(
                "UPDATE user SET givenname = :givenname, surname = :surname, email = :email, emailverified = :emailverified, modify_user = :modify_user, active_flag = :active_flag WHERE record_key = :record_key",
                UserParameterFactory.create(OperationType.UPDATE, callingId, user)
        );
        return findByKey(user.getKey());
    }

    /**
     * Deletes a User Model object.
     *
     * @param callingKey unique identifier of the calling user.
     * @param user       model object to be deleted.
     */
    @Override
    public User delete(final UUID callingKey, final User user) {
        if (callingKey == null) {
            throw new IllegalArgumentException("CallingKey is a required argument.");
        }
        if (user == null) {
            throw new IllegalArgumentException("User is a required argument.");
        }
        if (!userExists(user.getKey())) {
            throw new ModelNotFoundException("A User model cannot be found for the specified key, " + user.getKey().toString());
        }

        Long callingId = this.getRecordId(callingKey);
        jdbcTemplate.update(
                "UPDATE user SET active_flag = FALSE, modify_user = :modify_user WHERE record_key = :record_key",
                UserParameterFactory.create(OperationType.DELETE, callingId, user)
        );

        return findByKey(user.getKey());
    }

    /**
     * Retrieves a specified User Model object from persistent storage.
     *
     * @param key unique identifier of the object to retrieve.
     * @return populated User object.
     */
    @Override
    public User findByKey(final UUID key) {

        if (key == null) {
            throw new IllegalArgumentException("A model key is required.");
        }

        String sql = "SELECT record_key, givenname, surname, email, emailverified, active_flag FROM user WHERE record_key = :record_key";
        SqlParameterSource parameters = ModelParameterFactory.create(OperationType.SELECT, new User(key));
        List<User> userList = jdbcTemplate.query(sql, parameters, new UserRowMapper());
        if (userList == null || userList.size() == 0) {
            throw new ModelNotFoundException("A User model cannot be found for the specified key, " + key.toString());
        }
        return userList.get(0);
    }

    /**
     * @param email
     * @return
     */
    private boolean userExists(final String email) {
        String sql = "SELECT count(record_id) FROM user WHERE email = :email";
        Long userCount = jdbcTemplate.queryForObject(
                "SELECT count(record_id) FROM user WHERE email = :email",
                new MapSqlParameterSource("email", email),
                Long.class);
        return userCount > 0;
    }

    /**
     * @param key
     * @return
     */
    private boolean userExists(final UUID key) {
        Long userCount = jdbcTemplate.queryForObject(
                "SELECT count(record_id) FROM user WHERE record_key = :record_key",
                ModelParameterFactory.create(OperationType.SELECT, key),
                Long.class);
        return userCount > 0;
    }

}
