package com.egoshard.baliser.core.events;

import java.util.UUID;

/**
 *
 */
public class RequestEvent extends BaseEvent {

    UUID executorKey;

    /**
     * @param executorKey unique identifier of the user requesting access.
     */
    public RequestEvent(UUID executorKey) {
        this.executorKey = executorKey;
    }

    /**
     * @return
     */
    public UUID getExecutorKey() {
        return executorKey;
    }

}
