angular
    .module('baliserApp.dashboard', [
        'ui.router'
    ])

    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('dashboard', {
            url: '/',
            views: {
                "main": {
                    controller: 'DashCtrl',
                    templateUrl: 'dashboard/dashboard.tpl.html'
                }
            },
            data: { pageTitle: "Dashboard" }
        });
    }])

    .controller('DashCtrl', ['$scope', function DashCtrl($scope) {

    }]);