package com.egoshard.baliser.core.repository.jdbc.mappers.user;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.repository.utils.UuidUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 *
 */
public class UserRowMapper implements RowMapper<User> {

    /**
     * @param rs
     * @param i
     * @return
     * @throws SQLException
     */
    @Override
    public User mapRow(ResultSet rs, int i) throws SQLException {
        UUID key = UuidUtils.fromBytes(rs.getBytes("record_key"));
        User user = new User(key);
        user.setGivenName(rs.getString("givenname"));
        user.setSurname(rs.getString("surname"));
        user.setEmail(rs.getString("email"));
        user.setEmailVerified(rs.getBoolean("emailverified"));
        user.setActive(rs.getBoolean("active_flag"));
        return user;
    }

}
