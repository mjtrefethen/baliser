package com.egoshard.baliser.core.events;

import com.egoshard.baliser.core.domain.Model;

/**
 *
 */
public class ModelWrittenEvent<T extends Model> extends ResponseEvent {

    private final T model;

    /**
     * Base constructor for an immutable model written event.
     *
     * @param model newly written object.
     */
    public ModelWrittenEvent(final T model) {
        this.model = model;
    }

    /**
     * Retrieves the newly written entity.
     *
     * @return new entity.
     */
    public T getModel() {
        return model;
    }

}
