package com.egoshard.baliser.core.events;

import com.egoshard.baliser.core.domain.Model;

/**
 * Base return from a request for a single entity from persistence.
 */
public class ModelReadEvent<T extends Model> extends ResponseEvent {

    private final T model;

    /**
     * Constructor fo a generic read event.
     *
     * @param model model to return.
     */
    public ModelReadEvent(final T model) {
        this.model = model;
    }

    /**
     * Retrieves the retrieved model from the event.
     *
     * @return populated object extending Model.
     */
    public T getModel() {
        return model;
    }

}
