package com.egoshard.baliser.core.events;

import com.egoshard.baliser.core.domain.Model;

import java.util.UUID;

/**
 *
 */
public class WriteModelEvent<T extends Model> extends RequestEvent {

    private final T model;

    /**
     * Constructor to create an immutable event object.
     *
     * @param executorKey unique identifier for the user requesting access.
     * @param model       object to be created.
     */
    public WriteModelEvent(final UUID executorKey, final T model) {
        super(executorKey);
        this.model = model;
    }

    /**
     * Retrieves the model to be created.
     *
     * @return an object that extends from the Model base class.
     */
    public T getModel() {
        return model;
    }

}
