package com.egoshard.baliser.rest.domain.assembler;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.repository.utils.UuidUtils;
import com.egoshard.baliser.rest.controller.UserController;
import com.egoshard.baliser.rest.domain.user.UserResource;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 *
 */
public class UserAssembler {

    /**
     * @param model
     * @return
     */
    public static UserResource toResource(User model) {
        UserResource resource = new UserResource(model.getKey(), model.getName());
        resource.setGivenName(model.getGivenName());
        resource.setSurname(model.getSurname());
        resource.setEmail(model.getEmail());
        resource.setEmailVerified(model.isEmailVerified());
        resource.add(linkTo(methodOn(UserController.class).getForKey(UuidUtils.toNonDashString(model.getKey()))).withSelfRel());
        return resource;
    }

    /**
     * @param list
     * @return
     */
    public static List<UserResource> toResourceList(List<User> list) {
        List<UserResource> resources = new ArrayList<>();
        for (User user : list) {
            resources.add(toResource(user));
        }
        return resources;
    }

    /**
     * @param resource
     * @return
     */
    public static User toModel(UserResource resource) {
        User user = new User(resource.getKey());
        user.setGivenName(resource.getGivenName());
        user.setSurname(resource.getSurname());
        user.setEmail(resource.getEmail());
        user.setEmailVerified(resource.isEmailVerified());
        return user;
    }

    /**
     * @param list
     * @return
     */
    public static List<User> toModelList(List<UserResource> list) {
        List<User> users = new ArrayList<>();
        for (UserResource resource : list) {
            users.add(toModel(resource));
        }
        return users;
    }

}
