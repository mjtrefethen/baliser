package com.egoshard.baliser.core.service;

import com.egoshard.baliser.core.domain.user.User;
import com.egoshard.baliser.core.events.ModelReadEvent;
import com.egoshard.baliser.core.events.ModelWrittenEvent;
import com.egoshard.baliser.core.events.ReadModelEvent;
import com.egoshard.baliser.core.events.WriteModelEvent;
import com.egoshard.baliser.core.events.user.CreateUserEvent;

/**
 * Service interface for interacting with User Model objects.
 */
public interface UserService {

    /**
     * Mechanism to create a new user.
     *
     * @param event populated create event.
     * @return newly created Model object and associated key.
     */
    public ModelWrittenEvent<User> createUser(final CreateUserEvent event);

    /**
     * Request a user Model object by its unique identifier.
     *
     * @param event populated request event.
     * @return populated return event.
     */
    public ModelReadEvent<User> requestUser(final ReadModelEvent event);

    /**
     * Mechanism to update an existing user.
     *
     * @param event populated update event.
     * @return modified Model object and associated key.
     */
    public ModelWrittenEvent<User> updateUser(final WriteModelEvent<User> event);

    /**
     * Mechanism to delete an existing user.
     *
     * @param event populated delete event.
     * @return event indicating successful deletion..
     */
    public ModelWrittenEvent<User> deleteUser(final WriteModelEvent<User> event);

}