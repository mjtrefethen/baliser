package com.egoshard.baliser.rest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class JsonErrorInfo {

    private final Map<String, String> values;


    public JsonErrorInfo() {
        values = new HashMap<>();
    }

    /**
     * @param values
     */
    public JsonErrorInfo(Map<String, String> values) {
        this.values = values;
    }

    /**
     * @return
     */
    public Map<String, String> getValues() {
        return values;
    }

    /**
     * @return
     */
    public ModelAndView asModelAndView() {
        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
        return new ModelAndView(jsonView, values);
    }

}
