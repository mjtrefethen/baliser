package com.egoshard.baliser.core.repository.jdbc.mappers;

import com.egoshard.baliser.core.domain.Model;
import com.egoshard.baliser.core.repository.utils.UuidUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.util.UUID;

/**
 *
 */
public class ModelParameterFactory {

    protected static final UUID SYSTEM_KEY = UUID.fromString("4e08cf81-d452-4179-a9a2-06cd17ec7be5");

    /**
     * @param operation
     * @param model
     * @return
     */
    public static MapSqlParameterSource create(OperationType operation, Model model) {
        return create(UuidUtils.toBytes(model.getKey()));
    }

    /**
     * @param operation
     * @param key
     * @return
     */
    public static MapSqlParameterSource create(OperationType operation, UUID key) {
        return create(UuidUtils.toBytes(key));
    }

    /**
     * @param keyBytes
     * @return
     */
    private static MapSqlParameterSource create(byte[] keyBytes) {
        return new MapSqlParameterSource("record_key", keyBytes);
    }

}
