angular
    .module('baliserApp', [
        'ui.router',
        'ui.bootstrap',
        'templates-app',
        'templates-common',
        'baliserApp.dashboard',
        'baliserApp.devices',
        'baliserApp.groups',
        'baliserApp.zones'
    ])

    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    }])

    .run(function run() {
    })

    .controller('AppCtrl', ['$scope', '$location', function AppCtrl($scope, $location) {
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle + ' | Baliser';
            }
        });
    }]);