package com.egoshard.baliser.core.repository.jdbc.mappers;

/**
 *
 */
public enum OperationType {
    CREATE,
    UPDATE,
    SELECT,
    DELETE
}